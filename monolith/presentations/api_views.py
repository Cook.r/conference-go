from django.http import JsonResponse

from .models import Presentation
from events.models import Conference
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
import pika


class ConferenceEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class LocationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["name"]


class ApiListEncoder(ModelEncoder):
    model = Presentation
    
    properties = ["title", "status"]

    encoders = {
        "location": LocationListEncoder(),
        }
    
    def get_extra_data(self, o):
        return { "status": o.status.name}


class PresentationEncoder(ModelEncoder):
    model = Presentation
    properties = [ "presenter_name", "company_name", "presenter_email", "synopsis", "created"]
        
    def get_extra_data(self, o):
        return { "status": o.status.name}

    encoders = {
        "conference": ConferenceEncoder(),
    }





@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        presentations = [
            {
                "title": p.title,
                "status": p.status.name,
                "href": p.get_api_url()
            }
            for p in Presentation.objects.filter(conference=conference_id)
        ]

        return JsonResponse({"presentations": presentations}, encoder=ApiListEncoder, safe=False)
    
    else:
        content = json.loads(request.body)
        content["conference"] = Conference.objects.get(id=conference_id)

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        present = Presentation.objects.get(id=id)
        return JsonResponse(present, encoder=PresentationEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
        presentation,
        encoder=PresentationEncoder,
        safe=False,
        )



@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()

    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="approval")
    body = {"presenter_name": presentation.presenter_name, "presenter_email": presentation.presenter_email, "title": presentation.title}
    channel.basic_publish(exchange="", routing_key="approval", body=json.dumps(body),)
    connection.close()

    return JsonResponse(
        presentation,
        encoder=PresentationEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()

    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="rejection")
    content = {"presenter_name": presentation.presenter_name, "presenter_email": presentation.presenter_email, "title": presentation.title}
    channel.basic_publish(exchange="", routing_key="rejection", body=json.dumps(content),)
    connection.close()

    return JsonResponse(
        presentation,
        encoder=PresentationEncoder,
        safe=False,
    )
