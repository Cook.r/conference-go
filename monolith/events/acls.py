import requests
import json

from events.keys import PEXELS_API_KEY
from .keys import OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}"
    }
    
    url = "https://api.pexels.com/v1/search"
    
    response = requests.get(url, params=params, headers=headers)

    content = json.loads(response.content) 
    
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"Picture_url": None}


def get_weather_data(city, state):

    # get url 
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city}&appid=79c5ba4c15473ffd45042c76fc36a5b5"

    # make a response
    response = requests.get(url)

    # parse the response
    content = json.loads(response.content)
    # get lat and lon from content

    coords = {
       "lat": content[0]["lat"],
       "lon": content[0]["lon"]
    }
    
    # get url 
    current_url = f"https://api.openweathermap.org/data/2.5/forecast?q={city}&units=imperial&cnt=3&appid=79c5ba4c15473ffd45042c76fc36a5b5"

    # make respone
    full_response = requests.get(current_url)
 
    # parse the response
    contents = json.loads(full_response.content)
    # create weather dict
    weather = {
        "temp": contents["list"][0]["main"]["temp"],
        "weather": contents["list"][2]["weather"][0]["description"] 
    }
    # Return dict
    return weather
