from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

def Update_AccountVO(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.filter(updated__lt=updated).update_or_create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            is_active=is_active,
            updated=updated
        )
    else:
        if email:
            AccountVO.objects.filter(email=email).delete()


while True:
    try:
#       create the pika connection parameters
        parameters = pika.ConnectionParameters(host="rabbitmq")
#       create a blocking connection with the parameters
        connection = pika.BlockingConnection(parameters)

#       open a channel
        channel = connection.channel()
        channel.exchange_declare(exchange="account_info", exchange_type="fanout")
        print("Exchange declared")
        random = channel.queue_declare(queue="", exclusive=True)
#       declare a fanout exchange named "account_info"
        queue_name = random.method.queue

#       get the queue name of the randomly-named queue

#       bind the queue to the "account_info" exchange
        channel.queue_bind(exchange="account_info", queue=queue_name)
        print("Exchanged bound")
#       do a basic_consume for the queue name that calls function above
#       tell the channel to start consuming
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=Update_AccountVO,
            auto_ack=True,
            )
        print("basic consume")
        channel.start_consuming()
        print("Consuming")
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
