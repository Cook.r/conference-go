import json
import pika
import django
import os
import sys
import time
from pika.exceptions import AMQPConnectionError
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    # getting the data from body and converting it to a dict
    content = json.loads(body)

    print(body)
    # taking needed elements out of that data we just converted

    name = content["presenter_name"]
    email = content["presenter_email"]
    title = content["title"]

    #send_mail functions have to be in specific format. Should have looked through documentation more thoroughly 
    # pass in the elements we just got and using them to send mail

    send_mail(
        'Your presentation has been accepted',
        f'{name}, were happy to tell you that your presentation {title} has been accepted',
        'admin@conference.go',
        [{email}],
        fail_silently=False,)


def process_rejection(ch, method, properties, body):
    print(body)
    # getting the data from body and converting it to a dict
    content = json.loads(body)
    
    # taking needed elements out of that data we just converted
    name = content["presenter_name"]
    email = content["presenter_email"]
    title = content["title"]

    #send_mail functions have to be in specific format. Should have looked through documentation more thoroughly 
    # pass in the elements we just got and using them to send mail
    send_mail(
        'Your presentation has been rejected',
        f'{name}, were happy to tell you that your presentation {title} has been rejected',
        'admin@conference.go',
        [{email}],
        fail_silently=False,)

while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        #queueing names have to match queue names in the view function

        #queue for approval
        channel.queue_declare(queue='approval')
        channel.basic_consume(
            queue='approval',
            on_message_callback=process_approval,
            auto_ack=True,
        )

        #queue for rejection
        channel.queue_declare(queue='rejection')
        channel.basic_consume(
            queue='rejection',
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError: 
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
